#!/usr/bin/env bash

set -euo pipefail
set -xv

# maximum time to connect
CONNECT_TIMEOUT=${CONNECT_TIMEOUT:-4}
# allow a different DNS timeout
if [[ -z "${DNS_TIMEOUT:-}" ]]; then
  DNS_TIMEOUT=${CONNECT_TIMEOUT}
fi

if [[ "${1:-}" = "--iface" ]] ; then
	shift
	iface="$1"
	shift
	if ip link show dev $iface | grep -qF 'state UP' ; then
		source="$(ip -f inet addr show $iface | sed -En -e 's/.*inet ([0-9.]+).*/\1/p' | tail -n1)"
		if [ "$source" != "" ]; then
			source="-s $source"
		fi
	else
		exit 6
	fi
elif [[ "${1:-}" = "--addr" ]] ; then
	shift
	source="$1"
	shift
	if [ "$source" != "" ]; then
		source="-s $source"
	fi
fi

# First, check if we get passed --local-only
if [[ "${1:-}" = "--local-only" ]] && [[ -x "$(command -v is-local-net.sh)" ]]; then
    shift
    # in that case, check for match with local networks
    # note that we set a timeout so that we don't get stuck on host resolution
    timeout "$DNS_TIMEOUT" is-local-net.sh "$1" || exit 3
fi

if [[ -z "${NETCAT:-}" ]] ; then
  # prefer nmap version of ncat, if present
  NETCAT="$(command -v ncat || command -v nc || command -v nc.openbsd)"
fi
# hostname we wish to probe
HOSTNAME_CONNECT=$1
# port we wish to probe
PORT=$2
shift || exit 99
shift || exit 99

# try to connect to hostname:port, with a connect timeout of 3
# break away in 4 seconds from the outside if nc won't count connect_time into timeouts (I'm looking at you, Mac OS X)
timeout "$CONNECT_TIMEOUT" "$NETCAT" ${source:-} "$@" -w3 -z "$HOSTNAME_CONNECT" "$PORT" || exit 2
