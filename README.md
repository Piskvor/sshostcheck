# SSHostCheck

### Dynamically match SSH config from multiple options, by checking host at connect time, using an external script

## Use cases

- split DNS beyond user's control
- pre-connect scripts (e.g. port knocking)

# Dependencies

- a recent-ish OpenSSH client (`ssh`, usually - `ssh -V` should tell you; anything 8+ should do)
- ssh-keyscan (likewise a part of openssh-client)
- Optional, recommended: a netcat-like that accepts `-z` (widely available)

## Setup

- find your *target's* SSH hostkey fingerprint(s) (e.g. `ssh-keyscan myhost.example.com 22`), pick one
- make `ssh-check.sh` and/or `probe-port.sh` executable (and possibly put somewhere that's in $PATH)
- update your ssh_config with `Match` sections
  - We use a `Match` based on `Host` here, note that each Match directive repeats this check
  - First match is accepted, skips the remaining ones
  - In case no matching section is found, there's an `exec true` section which always matches
    - using a "default" configuration option would not work: it would actually clobber all the following ones

## Usage

- works automatically when using OpenSSH client (dropbear-client probably won't work, or libssh wrappers that do 
  their own config)
  - many toolchains can be reconfigured to use the OpenSSH executable instead of their internal SSH client (JetBrains, 
    MobaXTerm, xpra: usually there's some option like `--ssh=/usr/bin/ssh`)
- debug using verbose switches: `ssh -vvv myhost`

## Example ssh_config snippet for host "myhost"

```
    ## anywhere inside .ssh/config
    Host myhost
        ## THIS WOULD WORK TOO MUCH: would apply in *every* case
        # HostName myhost.example.com # we are replacing this hardcoded original, below

        ## rest of the host config        
        User minime
        Port 22
        # etc, SSH config options as usual... 
        
        ## we check for match, and eventually apply the per-match options
        # myhost.example.eu is reachable, has SSH *AND* has this specific hostkey
        Match host "myhost" exec "ssh-check.sh %h myhost.example.eu %p ssh-ed25519 AAAAAAAaaaaThisIsActuallyYourHostPubkeyNotRandomScreaming"
            HostName myhost.example.eu

        ## IFF the match fails, next one is checked
        # myhost.anothernet.example.org has *an* open port where SSH is trying to connect
        Match host "myhost" exec "probe-port.sh myhost.anothernet.example.org %p"
            HostName myhost.anothernet.example.org
            # NB: most SSH config options are acceptable here, HostName is just the usual case
            User foobar
            # e.g. in this case only connect using the specified key from agent
            IdentityFile /home/me/.ssh/foobar.pub
        
        ## we can also restrict to "only if host can be on the local network(s)" by --local-only
        ## *may* be faster, as this branch is discarded immediately if it doesn't fit the IPv4 address, without trying to connect
        # myhost.example.com is on the same network, reachable, has SSH *AND* has this specific hostkey
        Match host "myhost" exec "ssh-check.sh --local-only %h myhost.example.com %p ssh-ed25519 AAAAAAAaaaaThisIsActuallyAnotherHostkeyNotRandomScreaming"
            HostName myhost.example.com

        ## first success (return code 0) stops further matches
        # an IP address also works - note: %h is the alias, an address to connect needs to be given
        Match host "myhost" exec "ssh-check.sh %h 10.20.30.40 %p ssh-ed25519 AAAAAAAaaaaThisIsActuallyYourHostkeyNotRandomScreaming"
            HostName 10.20.30.40

        ## the exec can do anything, not just what our scripts check:
        # you could do tricks like "call a script, connect elsewhere", it just needs to return 0 on success
        Match host "myhost" exec "secretportknock-notarealscript-example.sh"
            HostName 192.168.1.1
            # again, we can set most ssh_config options per host+match 
            ConnectTimeout 30
            ConnectionAttempts 3

        ## this will always be executed, UNLESS a previous Match...matches.        
        Match host "myhost" exec "true"
            HostName myhost.some.other.network.example.co.uk
            
        ## fallback if none of the above works
        # this needs to be set, else ssh fails with error like "no HostName to connect"
        # needs to be below the Match sections, else it would set Hostname first, even though a Match would also apply
        # note that this will apply regardless of any Match-es - but only for directives that have not been set yet 
        Host myhost
            HostName myhost.some.other.network.example.com


    ## unrelated host(s)
    Host someotherhost
        # configuration of other hosts is not affected, because
        # "Match Restricts the following declarations (up to the next Host or Match keyword)"
        #   -man ssh_config        
        # as explained by @Noa Sakurajin at https://superuser.com/a/1426025
```
