#!/bin/sh

set -eu


CURRENT_PWD="$(dirname "$0")" && cd "${CURRENT_PWD}/.."
set -x

shellcheck --shell=bash ./**/*.sh ./*.sh
