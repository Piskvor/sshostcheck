#!/usr/bin/env bash

set -euo pipefail
## uncomment for debug verbosity
#set -xv

# maximum time to connect
CONNECT_TIMEOUT=${CONNECT_TIMEOUT:-4}
# allow a different DNS timeout
if [[ -z "${DNS_TIMEOUT:-}" ]]; then
  DNS_TIMEOUT=${CONNECT_TIMEOUT}
fi

# First, check if we get passed --local-only
if [[ "${1:-}" = "--local-only" ]] && [[ -x "$(command -v is-local-net.sh)" ]]; then
    shift
    # in that case, check for match with local networks
    # note that we set a timeout so that we don't get stuck on host resolution
    timeout "$DNS_TIMEOUT" is-local-net.sh "$2" || exit 3
    CONNECT_TIMEOUT=1
fi

if [[ -z "${NETCAT:-}" ]] ; then
  # prefer nmap version of ncat, if present
  NETCAT="$(command -v ncat || command -v nc || command -v nc.openbsd)"
fi
# we currently don't care what alias we are passed
# shellcheck disable=SC2034
HOSTNAME_ALIAS=$1
# hostname we wish to probe
HOSTNAME_CONNECT=$2
# port we wish to probe
PORT=$3
shift
shift
shift
HOST_KEY=$*
shift || exit 99

if [[ -n "$NETCAT" ]]; then
  # try to connect to hostname:port, with a connect timeout of 3
  # break away in 4 seconds from the outside if nc won't count connect_time into timeouts (I'm looking at you, Mac OS X)
  timeout "$CONNECT_TIMEOUT" "$NETCAT" -w3 -z "$HOSTNAME_CONNECT" "$PORT" 2>/dev/null || exit 2
else
  # no netcat? try busybox
  if [[ -x "$(command -v busybox)" ]]; then
    NETCAT="$(command -v busybox) nc"
    timeout "$CONNECT_TIMEOUT" "$NETCAT" -z "$HOSTNAME_CONNECT" "$PORT" 2>/dev/null || exit 2
  else
    # At least ping the host, exit if error
    timeout "$CONNECT_TIMEOUT" ping -qn "$HOSTNAME_CONNECT" -c1 -t3 -o >/dev/null || exit 2
  fi
fi

# NOTE: ssh-keyscan doesn't use ssh_config(5), it connects directly
# get hostkeys, see if we can find the one we got passed
ssh-keyscan "-T$CONNECT_TIMEOUT" "-p$PORT" "$HOSTNAME_CONNECT" 2>/dev/null | grep -Fq "$HOST_KEY" || exit 4
