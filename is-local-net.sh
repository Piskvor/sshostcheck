#!/usr/bin/env bash

# return 0 if passed host is local (w/r/t interfaces), 3 if not local
# NB: this only checks for a network range match, i.e. 192.168.0.1 will match as local in MANY places

set -euo pipefail
## uncomment for debug verbosity
#set -xv

## this should always time out - debug
#dig $1 @blackhole.webpagetest.org

# shellcheck disable=SC2001
get_host() {
  echo "$1" | sed 's/\.[0-9]\+$/./'
}

if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
  HOST_ADDR=$1
else
  HOST_ADDR="$(host "$1" | grep -F 'has address' | head -n1 | sed 's/.*has address //')"
fi
HOST_NET="$(get_host "$HOST_ADDR")"

for local in $(hostname -I) ; do
  LOCAL_NET="$(get_host "$local")"
  if [[ "$LOCAL_NET" = "$HOST_NET" ]]; then
    exit 0
  fi
done

exit 3
