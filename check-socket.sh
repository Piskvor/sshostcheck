#!/bin/bash

# return 0 if we have a controlpath, so we don't even care where we're connecting to
# caveat autossh - this may create strange loops!

set -euo pipefail
set -xv

# maximum time to connect
# bashsupport disable=BP5006
CONNECT_TIMEOUT=${CONNECT_TIMEOUT:-4}
# hostname we wish to probe

hostname_connect="$1"

# yes, even ssh -G uses Match, so may time out
control_path="$(timeout "$CONNECT_TIMEOUT" ssh -G "$hostname_connect" | grep controlpath | cut "-d " -f2)"

if [ -z "$control_path" ]; then
  exit 1
fi

if [ ! -S "$control_path" ]; then
  exit 4
fi

# don't hang forever
if timeout "$CONNECT_TIMEOUT" ssh -o "ControlPath=$control_path" -O check 'bogus-hostname-check' ; then
  # IT'S ALIVE
  exit 0
else
  # perhaps delete an unused socket?
  exit 2
fi